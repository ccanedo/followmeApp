package com.example.cristiancanedo.followmeapp32.Recursos;

/**
 * Created by andresbolado on 06/07/17.
 */

public class indications {

    /*private String casaTosomewhere = "{\"steps\":[{\"start_location\":{\"lat\": 21.936466,\"lng\": -102.298843}, \"end_location\":{\"lat\": 21.9365122, \"lng\": -102.2986871}, \"html_instructions\":\"Dirigete hacia el Este sobre calle Diaz.\"}, {\"start_location\":{\"lat\":21.9365122, \"lng\": -102.2986871}, \"end_location\":{\"lat\": 21.9368888, \"lng\": -102.2986645}, \"html_instructions\":\"Gira a la derecha hacia el norte hasta llegar a Diaz.\"}, {\"start_location\": {\"lat\": 21.9368888, \"lng\":-102.2986645}, \"end_location\":{\"lat\": 21.9370455, \"lng\": -102.2984287}, \"html_instructions\":\"gira a la derecha ahi esta tu destino\"} ]}";
    private String casaTosoriana = "";
    private String casaTouaa = "";
    private String casaTodif = "";
    private String sorianaTocasa = "";
    private String sorianaTouaa = "";*/
    private String sorianaTodif = "{\"steps\":[{\"start_location\": {\"lat\": 25.844429, \"lng\": -97.454422}, \"end_location\":{\"lat\": 25.844438, \"lng\": -97.454243}, \"html_instructions\":\"Estas en electromecanica, dirigete hacia el oeste hasta el siguiente punto.\", \"distance\":{\"radius\": 5, \"value\": 50}}, {\"start_location\": {\"lat\": 25.844438, \"lng\": -97.454243}, \"end_location\":{\"lat\": 25.843905, \"lng\": -97.454281}, \"html_instructions\":\"Dar vuelta a mano derecha y caminar, Hasta el siguiente punto.\", \"distance\":{\"radius\": 5, \"value\": 50}}, {\"start_location\": {\"lat\": 25.843905, \"lng\": -97.454281}, \"end_location\":{\"lat\": 25.843693, \"lng\": -97.454276}, \"html_instructions\":\"Seguir caminando de frente. a tu mano izquierda están los T y a tu derecha salones CC.\", \"distance\":{\"radius\":5, \"value\": 100}}, {\"start_location\": {\"lat\": 25.843693, \"lng\": -97.454276}, \"end_location\":{\"lat\": 25.843331, \"lng\": -97.454308}, \"html_instructions\":\"Salones Q y sigue caminando de frente.\", \"distance\":{\"radius\": 5, \"value\": 200}}, {\"start_location\": {\"lat\": 25.843331, \"lng\": -97.454308}, \"end_location\":{\"lat\": 25.843307, \"lng\": -97.454050}, \"html_instructions\":\"Dar vuelta a mano izquierda y seguir caminando.\", \"distance\":{\"radius\": 5, \"value\": 50}}, {\"start_location\":{\"lat\": 25.843307, \"lng\": -97.454050}, \"end_location\":{\"lat\": 25.843037, \"lng\":  -97.454055}, \"html_instructions\":\"Gira a la derecha y sigue caminando.\", \"distance\":{\"radius\":5, \"value\": 60}}, { \"start_location\":{\"lat\": 25.843037, \"lng\":  -97.454055}, \"end_location\":{\"lat\": 25.842868, \"lng\": -97.454065},\"html_instructions\":\"Sigue caminando de frente y a tu mano derecha está la cafetería.\", \"distance\":{\"radius\": 5, \"value\": 30}}, { \"start_location\":{\"lat\": 25.842868, \"lng\": -97.454065}, \"end_location\":{\"lat\": 25.842861, \"lng\": -97.453853}, \"html_instructions\":\"Dar vuelta a mano izquierda y caminar.\", \"distance\":{\"radius\": 5, \"value\": 30}}, { \"start_location\":{\"lat\": 25.842861, \"lng\": -97.453853}, \"end_location\":{\"lat\": 25.842861, \"lng\": -97.453848}, \"html_instructions\":\"Dar vuelta a mano izquierda y quedaras de frente a la biblioteca.\", \"distance\":{\"radius\": 5, \"value\": 30}} ]}";
   /* private String uaaTocasa = "";
    private String uaaTosoriana = "";
    private String uaaTodif = "";*/
    private String difTosoriana = "{\"steps\":[{\"start_location\": {\"lat\": 25.842981, \"lng\": -97.453835}, \"end_location\":{\"lat\": 25.842851, \"lng\": -97.453856}, \"html_instructions\":\"Estas en la biblioteca, dirigete hacia el sur hasta el siguiente punto.\", \"distance\":{\"radius\": 10, \"value\": 50}}, {\"start_location\": {\"lat\": 25.842851, \"lng\": -97.453856}, \"end_location\":{\"lat\": 25.842865, \"lng\": -97.454060}, \"html_instructions\":\"Dar vuelta a mano derecha y caminar, Hasta el siguiente punto.\", \"distance\":{\"radius\": 5, \"value\": 50}}, {\"start_location\": {\"lat\": 25.842865, \"lng\": -97.454060}, \"end_location\":{\"lat\": 25.843056, \"lng\": -97.454063}, \"html_instructions\":\"Dar vuelta a la derecha y seguir caminando.\", \"distance\":{\"radius\":5, \"value\": 100}}, {\"start_location\": {\"lat\": 25.843056, \"lng\": -97.454063}, \"end_location\":{\"lat\": 25.843305, \"lng\": -97.454042}, \"html_instructions\":\"Seguir caminando de frente. a tu mano izquierda está la cafeteria.\", \"distance\":{\"radius\": 5, \"value\": 200}}, {\"start_location\": {\"lat\": 25.843305, \"lng\": -97.454042}, \"end_location\":{\"lat\": 25.843334, \"lng\": -97.454302}, \"html_instructions\":\"Dar vuelta a la izquierda y seguir caminando.\", \"distance\":{\"radius\": 5, \"value\": 50}}, {\"start_location\":{\"lat\": 25.843334, \"lng\": -97.454302}, \"end_location\":{\"lat\": 25.843554, \"lng\":  -97.454275}, \"html_instructions\":\"Dar vuelta a mano derecha y seguir caminando.\", \"distance\":{\"radius\":5, \"value\": 60}}, { \"start_location\":{\"lat\": 25.843554, \"lng\":  -97.454275}, \"end_location\":{\"lat\": 25.843993, \"lng\": -97.454270},\"html_instructions\":\"has llegado a los salones Q, sigue caminando por el pasillo.\", \"distance\":{\"radius\": 5, \"value\": 30}}, { \"start_location\":{\"lat\": 25.843993, \"lng\": -97.454270}, \"end_location\":{\"lat\": 25.844425, \"lng\": -97.454243}, \"html_instructions\":\"Sigue caminando de frente y a tu mano derecha están los salones CC y al lado izquierdo el edificio T.\", \"distance\":{\"radius\": 5, \"value\": 30}}, { \"start_location\":{\"lat\": 25.844425, \"lng\": -97.454243}, \"end_location\":{\"lat\": 25.844439, \"lng\":  -97.454324}, \"html_instructions\":\"Dar vuelta a mano izquierda y estaras de frente a los salones de Electromecanica.\", \"distance\":{\"radius\": 5, \"value\": 30}} ]}";




    /*private String difTocentro = "{\"steps\":[{\"start_location\":{\"lat\":21.909174,\"lng\":-102.3116296}, \"end_location\":{\"lat\":21.9092411, \"lng\": -102.3111106}, \"html_instructions\": \"Dirigete hacia el Este por Guadalupe Gonzale hacia la avenida Universidad.\", \"distance\":{\"radius\":40, \"value\":77 }}, {\"start_location\":{\"lat\":21.9092411,\"lng\":-102.3111106}, \"end_location\":{\"lat\":21.9085269, \"lng\":-102.3105612}, \"html_instructions\": \"Al llegar al cruze con avenida universidad, gire a la derecha hacia el sur sobre avenida universidad.\", \"distance\":{\"radius\":30, \"value\":120}}, {\"start_location\":{\"lat\":21.9085269,\"lng\":-102.3105612}, \"end_location\":{\"lat\":21.896502, \"lng\": -102.304328}, \"html_instructions\": \"unos metros en frente encontraras una parada de camiones, aguarda ahí las ruta numero Once. Esta te llevara al centro.\", \"distance\":{\"radius\":150, \"value\": 700}}, {\"start_location\":{\"lat\":21.896502,\"lng\":-102.304328}, \"end_location\":{\"lat\":21.898518, \"lng\":-102.296158 }, \"html_instructions\": \"El camion girará a la izquierda en la siguiente calle, vas por buen camino.\", \"distance\":{\"radius\":150, \"value\": 400}}, {\"start_location\":{\"lat\":21.898518,\"lng\":-102.296158}, \"end_location\":{\"lat\":21.891549, \"lng\":-102.295063}, \"html_instructions\": \"En la proxima avenida el camion girara a la derecha hacia el sur, por avenida independencia.\", \"distance\":{\"radius\":150, \"value\":100}}, {\"start_location\":{\"lat\":21.891549,\"lng\":-102.295063}, \"end_location\":{\"lat\":21.889976, \"lng\":-102.293432}, \"html_instructions\": \"El camion seguira por calle mexico hasta la calle General José María Arteaga\", \"distance\":{\"radius\":150, \"value\":80}}, {\"start_location\":{\"lat\":21.889976,\"lng\":-102.293432}, \"end_location\":{\"lat\":21.886850, \"lng\":-102.296249 }, \"html_instructions\": \"El camion girara a la derecha por general José María Arteaga. Ya casi llegas a tu destino.\", \"distance\":{\"radius\":150, \"value\":200 }}, {\"start_location\":{\"lat\":21.886850,\"lng\":-102.296249}, \"end_location\":{\"lat\":21.884172, \"lng\":-102.295444 }, \"html_instructions\": \"En la siguiente calle el camion girará a la izquierda sobre avenida José María Morelos y pavón, esté atento\", \"distance\":{\"radius\":150, \"value\":400 }}, {\"start_location\":{\"lat\":21.884172,\"lng\":-102.295444}, \"end_location\":{\"lat\":21.883774, \"lng\": -102.295406}, \"html_instructions\": \"En la proxima parada del camion, BAJATE, despues te dirije hacia el sur con direccion a calle pedro parga\", \"distance\":{\"radius\": 50, \"value\": 90}}, {\"start_location\":{\"lat\":21.883774,\"lng\":-102.295406}, \"end_location\":{\"lat\":21.883575, \"lng\":-102.295363 }, \"html_instructions\": \"Al llegar a la calle pedro parga espera hasta que el semaforo este en rojo, cruza la calle y estaras en el lado sur este del Parian.\", \"distance\":{\"radius\":30, \"value\": 200}}]}";
    private String difTouaa = "{\"steps\":[{\"start_location\":{\"lat\":21.909174,\"lng\":-102.3116296}, \"end_location\":{\"lat\":21.909507, \"lng\":-102.311586}, \"html_instructions\":\"Dirigete hacia el norte, cruza la avenida Guadalupe Gonzalez\", \"distance\":{\"radius\":30,\"value\":40}}, {\"start_location\":{\"lat\":21.909507, \"lng\":-102.311586}, \"end_location\":{\"lat\":21.909528, \"lng\":-102.314646}, \"html_intructions\":\"sube la acera y gira a la derecha sobre avenida Guadalupe Gonzalez llendo hacia el Oeste\",\"distance\":{\"radius\":30, \"value\":200}}, {\"start_location\":{\"lat\":21.909528, \"lng\":-102.314646}, \"end_location\":{\"lat\":21.910369, \"lng\":-102.314737}, \"html_instructions\":\"Unos metros adelante justo a tu derecha encontraras la puerta sur de la universidad, una ves que llegues gira a la derecha y continua por la acera.\", \"distance\":{\"radius\":30, \"value\":50}}, {\"start_location\":{\"lat\":21.910369, \"lng\":-102.314737}, \"end_location\":{\"lat\":21.910404, \"lng\":-102.315730}, \"html_instructions\":\"Gira a la izquierda con direccion al oeste, tendras que cruzar una pequeña avenida así que toma tus precauciones. Tu destino estara justo en frente, para el tendras que cruzar la avenida y despues pasar por algunos pasillos\", \"distance\":{\"radius\":40, \"value\":150}}]}";
*/

    public indications() {}
    /*public String getCasaTosomewhere() {return casaTosomewhere;}
    public String getCasaToSoriana() {
        return casaTosoriana;
    }
    public String getCasaToUaa() {
        return casaTouaa;
    }
    public String getCasaToDif() {
        return casaTodif;
    }
    public String getSorianaToCasa() {
        return sorianaTocasa;
    }
    public String getSorianaToUaa() {
        return sorianaTouaa;
    }*/
    public String getSorianaToDif() {
        return sorianaTodif;
    }
    /*public String getUaaToCasa() {
        return uaaTocasa;
    }
    public String getUaaToSoriana() {
        return uaaTosoriana;
    }
    public String getUaaToDif() {
        return uaaTodif;
    }*/
    public String getDifToSoriana() {
        return difTosoriana;
    }
   /* public String getDifToCentro() {
        return difTocentro;
    }
    public String getDifToUaa() {
        return difTouaa;
    }*/
}
