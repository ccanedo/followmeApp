package com.example.cristiancanedo.followmeapp32;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cristiancanedo.followmeapp32.Recursos.AddressModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, LocationListener{


    ListView lv;
    ArrayList<String> lugares = new ArrayList<>();
    ArrayList<AddressModel> address = new ArrayList<>();
    ArrayAdapter<String> adapter;

    Button btnAcerca;

    boolean one = true;
    boolean ena = false;
    ProgressDialog dialog;

    LocationManager locationManager;

    private static final int MY_DATA_CHECK_CODE = 1;
    private TextToSpeech mTts;

    AddressModel currenAddres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e(">>>", "hjhkjhkjhjkhjhjhkjhkjhjhj");

        btnAcerca = (Button) findViewById(R.id.btnAcerca);

        btnAcerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, AcercaDe.class);
                startActivity(i);

            }
        });


        dialog = ProgressDialog.show(MainActivity.this, "Espera.",
                "Estamos buscando tu ubicacion", true);

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);

        AddressModel am = new AddressModel();
        am.setLatitude(25.844429);
        am.setLongitude(-97.454422);
        am.setId(1);
        am.setAddress("");
        am.setTitle("Electromecanica");

        AddressModel am2 = new AddressModel();
        am2.setLatitude(25.842981);
        am2.setLongitude(-97.453835);
        am2.setId(2);
        am2.setAddress("");
        am2.setTitle("Biblioteca");

        address.add(am);
        address.add(am2);

        for (int i = 0; i < address.size(); i++) {

            lugares.add(address.get(i).getTitle());

        }


        lv = (ListView) findViewById(R.id.lvLugares);

        adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_expandable_list_item_1, lugares);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (ena) {

                    if(i + 1 != currenAddres.getId()) {

                        locationManager.removeUpdates(MainActivity.this);

                        Intent in = new Intent(MainActivity.this, MapsActivity.class);

                        in.putExtra("title", address.get(i).getTitle());
                        in.putExtra("latitude", address.get(i).getLatitude());
                        in.putExtra("longitude", address.get(i).getLongitude());
                        in.putExtra("id", address.get(i).getId());

                        in.putExtra("title2", currenAddres.getTitle());
                        in.putExtra("latitude2", currenAddres.getLatitude());
                        in.putExtra("longitude2", currenAddres.getLongitude());
                        in.putExtra("id2", currenAddres.getId());
                        startActivity(in);
                    }else{

                        Toast.makeText(MainActivity.this, "Estas en ese punto", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    Toast.makeText(MainActivity.this, "Estas lejos de un punto", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);


        }else{

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        }
    }

    private double distance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 6371;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        return dist; // output distance, in Kilometers
    }


    @Override
    public void onInit(int i) {

        String myText = "Bienvenido, selecciona el lugar al que deceas ir.";
        mTts.speak(myText, TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public void onLocationChanged(Location location) {

        for (int i = 0; i < address.size(); i++) {
            Log.e(">>>", distance(location.getLatitude(), location.getLongitude(), address.get(i).getLatitude(), address.get(i).getLongitude()) + " " + address.get(i).getTitle());
            if (distance(location.getLatitude(), location.getLongitude(), address.get(i).getLatitude(), address.get(i).getLongitude()) <= 0.050) {

                ena = true;

                currenAddres = address.get(i);

                if(one) {
                    dialog.dismiss();
                    one = false;
                }
            }
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

        Toast.makeText(this, "Tu localizacion se ha activado.", Toast.LENGTH_SHORT).show();
        onResume();


    }

    @Override
    public void onProviderDisabled(String s) {

        Toast.makeText(this, "Tu localizacion esta Desactivada.", Toast.LENGTH_SHORT).show();

    }
}
