package com.example.cristiancanedo.followmeapp32.Recursos;

/**
 * Created by andresbolado on 29/06/17.
 */

public class StepsModel {

    private int distance;
    private String instruction;
    private double init_latitud, init_longitud, end_latitud, end_longitud, radius;

    public StepsModel(){
        this.distance = 0;
        this.instruction = "";
        this.init_latitud = 0.0;
        this.init_longitud = 0.0;
        this.end_latitud = 0.0;
        this.end_longitud = 0.0;
        this.radius = 0.0;
    }

    public double getRadius() {return radius;}

    public void setRadius(double radius) {this.radius = radius;}

    public int getDistance() {return distance;}

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public double getInit_latitud() {
        return init_latitud;
    }

    public void setInit_latitud(double init_latitud) {
        this.init_latitud = init_latitud;
    }

    public double getInit_longitud() {
        return init_longitud;
    }

    public void setInit_longitud(double init_longitud) {
        this.init_longitud = init_longitud;
    }

    public double getEnd_latitud() {
        return end_latitud;
    }

    public void setEnd_latitud(double end_latitud) {
        this.end_latitud = end_latitud;
    }

    public double getEnd_longitud() {
        return end_longitud;
    }

    public void setEnd_longitud(double end_longitud) {
        this.end_longitud = end_longitud;
    }
}
