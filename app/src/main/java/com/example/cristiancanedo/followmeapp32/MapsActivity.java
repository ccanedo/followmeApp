package com.example.cristiancanedo.followmeapp32;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.cristiancanedo.followmeapp32.Recursos.AddressModel;
import com.example.cristiancanedo.followmeapp32.Recursos.StepsModel;
import com.example.cristiancanedo.followmeapp32.Recursos.indications;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, TextToSpeech.OnInitListener, LocationListener {

    private GoogleMap mMap;

    Button btnBack;
    AddressModel comingAddres, intiAddress;

    LocationManager locationManager;

    ProgressDialog dialog;

    int seconds = 0;

    boolean one = false;

    private static final int MY_DATA_CHECK_CODE = 1;
    private TextToSpeech mTts;

    ArrayList<StepsModel> stepsList = new ArrayList<>();

    int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        dialog = ProgressDialog.show(MapsActivity.this, "Espera.",
                "Estamos buscando tu ubicacion", true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);

        btnBack = (Button) findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                locationManager.removeUpdates(MapsActivity.this);
                finish();

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        } else {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:

                if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                    // success, create the TTS instance
                    try {

                        mTts = new TextToSpeech(this, this);
                    } catch (Exception e) {

                        Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // missing data, install it
                    Intent installIntent = new Intent();
                    installIntent.setAction(
                            TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    startActivity(installIntent);
                }

                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                Toast.makeText(MapsActivity.this, stepsList.get(cont).getInstruction(), Toast.LENGTH_SHORT).show();

            }
        });


        comingAddres = new AddressModel();
        intiAddress = new AddressModel();

        String title = getIntent().getStringExtra("title");
        double latitude = getIntent().getDoubleExtra("latitude", 0.0);
        double longitude = getIntent().getDoubleExtra("longitude", 0.0);
        int id = getIntent().getIntExtra("id", 0);

        String title2 = getIntent().getStringExtra("title2");
        double latitude2 = getIntent().getDoubleExtra("latitude2", 0.0);
        double longitude2 = getIntent().getDoubleExtra("longitude2", 0.0);
        int id2 = getIntent().getIntExtra("id2", 0);

        comingAddres.setTitle(title);
        comingAddres.setLongitude(longitude);
        comingAddres.setLatitude(latitude);
        comingAddres.setId(id);

        intiAddress.setTitle(title2);
        intiAddress.setLongitude(longitude2);
        intiAddress.setLatitude(latitude2);
        intiAddress.setId(id2);


        String code = intiAddress.getId() + "To" + comingAddres.getId();
        switch (code) {


            case "1To2":

                try {
                    JSONObject josnUno = new JSONObject(new indications().getSorianaToDif());
                    JSONArray steps = josnUno.getJSONArray("steps");

                    for (int i = 0; i < steps.length(); i++) {

                        JSONObject obj = steps.getJSONObject(i);

                        JSONObject endLoc = obj.getJSONObject("end_location");
                        JSONObject initLoc = obj.getJSONObject("start_location");
                        JSONObject distance = obj.getJSONObject("distance");
                        String inst = obj.getString("html_instructions");
                        String instClear = Html.fromHtml(inst).toString();

                        double initLat = initLoc.getDouble("lat");
                        double initLon = initLoc.getDouble("lng");
                        double endLat = endLoc.getDouble("lat");
                        double endLon = endLoc.getDouble("lng");
                        int value = distance.getInt("value");
                        double radius = distance.getInt("radius");

                        LatLng inicio = new LatLng(initLat, initLon);
                        LatLng fin = new LatLng(endLat, endLon);

                        StepsModel stepsmodel = new StepsModel();
                        stepsmodel.setInstruction(instClear);
                        stepsmodel.setInit_longitud(initLon);
                        stepsmodel.setInit_latitud(initLat);
                        stepsmodel.setEnd_latitud(endLat);
                        stepsmodel.setEnd_longitud(endLon);
                        stepsmodel.setDistance(value);
                        stepsmodel.setRadius(radius);

                        stepsList.add(stepsmodel);

                        PolylineOptions polo = new PolylineOptions();
                        polo.add(inicio, fin);
                        polo.width(5);
                        polo.color(Color.BLUE);

                        mMap.addPolyline(polo);

                        //map.addPolyline(new PolylineOptions().add(new LatLng(51.5, -0.1), new LatLng(40.7, -74.0)).width(5).color(Color.RED));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case "2To1":

                try {
                    JSONObject josnUno = new JSONObject(new indications().getDifToSoriana());
                    JSONArray steps = josnUno.getJSONArray("steps");

                    for (int i = 0; i < steps.length(); i++) {

                        JSONObject obj = steps.getJSONObject(i);

                        JSONObject endLoc = obj.getJSONObject("end_location");
                        JSONObject initLoc = obj.getJSONObject("start_location");
                        JSONObject distance = obj.getJSONObject("distance");
                        String inst = obj.getString("html_instructions");
                        String instClear = Html.fromHtml(inst).toString();

                        double initLat = initLoc.getDouble("lat");
                        double initLon = initLoc.getDouble("lng");
                        double endLat = endLoc.getDouble("lat");
                        double endLon = endLoc.getDouble("lng");
                        int value = distance.getInt("value");
                        double radius = distance.getInt("radius");

                        LatLng inicio = new LatLng(initLat, initLon);
                        LatLng fin = new LatLng(endLat, endLon);

                        StepsModel stepsmodel = new StepsModel();
                        stepsmodel.setInstruction(instClear);
                        stepsmodel.setInit_longitud(initLon);
                        stepsmodel.setInit_latitud(initLat);
                        stepsmodel.setEnd_latitud(endLat);
                        stepsmodel.setEnd_longitud(endLon);
                        stepsmodel.setDistance(value);
                        stepsmodel.setRadius(radius);

                        stepsList.add(stepsmodel);

                        PolylineOptions polo = new PolylineOptions();
                        polo.add(inicio, fin);
                        polo.width(5);
                        polo.color(Color.BLUE);

                        mMap.addPolyline(polo);

                        //map.addPolyline(new PolylineOptions().add(new LatLng(51.5, -0.1), new LatLng(40.7, -74.0)).width(5).color(Color.RED));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            default:

                Toast.makeText(this, "No hay un recorrido disponible para estos lugares", Toast.LENGTH_SHORT).show();
                break;
        }

        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude2, longitude2), 15));
    }

    private double distance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 6371;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        return dist; // output distance, in Kilometers
    }

    @Override
    public void onLocationChanged(Location location) {


        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);

        try {
            dialog.dismiss();
            Log.w(">>> Distance: ", (int) (distance(location.getLatitude(), location.getLongitude(), stepsList.get(cont).getEnd_latitud(), stepsList.get(cont).getEnd_longitud()) * 1000) + " metros, R: " + stepsList.get(cont).getRadius() / 1000);
            // Toast.makeText(this, (int) (distance(location.getLatitude(), location.getLongitude(), stepsList.get(cont).getEnd_latitud(), stepsList.get(cont).getEnd_longitud()) * 1000)+" metros, "+stepsList.get(cont).getRadius(), Toast.LENGTH_SHORT).show();


        } catch (Exception e) {


        }


        if (distance(location.getLatitude(), location.getLongitude(), stepsList.get(cont).getEnd_latitud(), stepsList.get(cont).getEnd_longitud()) <= (stepsList.get(cont).getRadius() / 1000)) {

            try {
                //Toast.makeText(MapsActivity.this, stepsList.get(cont + 1).getInstruction(), Toast.LENGTH_SHORT).show();

                String myText = stepsList.get(cont + 1).getInstruction();
                mTts.speak(myText, TextToSpeech.QUEUE_FLUSH, null);
                cont += 1;

            } catch (Exception e) {


                if (distance(location.getLatitude(), location.getLongitude(), comingAddres.getLatitude(), comingAddres.getLongitude()) <= 0.005){

                Toast.makeText(MapsActivity.this, "has llegado a tu destino", Toast.LENGTH_SHORT).show();
                locationManager.removeUpdates(this);
                finish();

            }
        }
        }

        if(distance(location.getLatitude(), location.getLongitude(), intiAddress.getLatitude(), intiAddress.getLongitude()) <= 0.005 && one){

            dialog.dismiss();
            String myText = stepsList.get(0).getInstruction();
            mTts.speak(myText, TextToSpeech.QUEUE_FLUSH, null);
            one = false;

        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

        Toast.makeText(this, "Tu localizacion se ha activado.", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        dialog.dismiss();


    }

    @Override
    public void onProviderDisabled(String s) {

        dialog = ProgressDialog.show(MapsActivity.this, "Espera.",
                "Estamos buscando tu ubicacion", true);
        Toast.makeText(this, "Tu localizacion esta Desactivada.", Toast.LENGTH_SHORT).show();



    }

    @Override
    public void onInit(int i) {

        one = true;


    }
}
