package com.example.cristiancanedo.followmeapp32.Recursos;

/**
 * Created by andresbolado on 29/06/17.
 */

public class AddressModel {

    private String title;
    private double latitude;
    private double longitude;
    private String address;
    private int id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AddressModel(){

        this.title = "";
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.address = "";
        this.id = 0;
    }


}
